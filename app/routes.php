<?php

//  Home page {Redirect}
$app->get('/', function($request, $response, $args) {

    // Render twig template
    return $this->view->render($response, 'home.twig', ['ray'=>'Simple Example']);

})->setName('home');

//  Home page {Redirect}
$app->get('/hello/', function($request, $response, $args) {

    // Render twig template
    return $this->view->render($response, 'home.twig', ['ray'=>'Simple Example']);

})->setName('hhello');


